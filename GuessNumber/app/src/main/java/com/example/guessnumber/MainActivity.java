package com.example.guessnumber;

import java.util.Random;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


@SuppressLint("ShowToast") public class MainActivity extends Activity implements OnClickListener {
		
    private int tries = 0;
    private int levels =1;
    private int rangeValue = 0;
    private  int correctAnswers =0;
    
    private TextView userDisplayArea; 
    private Button startButton;
    private Button okButton;
    private Button restartButton;
    private TextView level;
    private TextView range;
    private TextView livesRemaining;
    private EditText userInputFiled;
    
    private void initButtons(){
    	
    	    userDisplayArea = (TextView) findViewById(R.id.textArea);
    	    userInputFiled = (EditText) findViewById(R.id.user_input);
    	    startButton = (Button) findViewById(R.id.start_game_btn);
    	    okButton = (Button) findViewById(R.id.Button_ok);
    	    restartButton = (Button) findViewById(R.id.btn_restart);
    	    level = (TextView) findViewById(R.id.level_txt);
    	    range = (TextView) findViewById(R.id.range_txt);
    	    livesRemaining = (TextView) findViewById(R.id.lives_xtx);
    }

    private  int randomNumber = 0;
    
    private Random rnd = new Random();

    public int getRange() {
        return rangeValue;
    }

    public int getTries() {
        return tries;
    }
    
    private int getRandomNumber() {
        return randomNumber;
    }
    private void setRandomNumber(){
    	randomNumber = rnd.nextInt(getRange());
    }
    
    

    private void setTries(int tries) {
        this.tries = tries;
    }

    private void setRange(int diapazon) {
        this.rangeValue = diapazon;
    }
    
    public String adviceUser(int userInput) {
        int diff = Math.abs(getRandomNumber() - userInput);
        if (diff <= 5) {
            if (userInput > getRandomNumber()) {
                return "Close,but your value is BIGGER than secret number ";
            }
                return "Close,but your value is SMALLER than secret number";

        }
            else if (diff <= 10) {
                return "Your number is +- 10 of secret number";
            }

            else if (diff <=20){
            return "Your number is +- 20 of secret number";
        }

            return userInput + " Your number is not close to secret number";
        }
    
    public void setDefaultValues(){
    	levels = 0;
        setRange(50);
        setTries(5);
    }
    
    private void startNewLevel(){   
        correctAnswers++;
        setDefaultValues();
        levels = correctAnswers;
        setRandomNumber();
        setRange(getRange() + correctAnswers * 25);
        setTries(getTries() + correctAnswers);
    }

    


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initButtons();      
        okButton.setOnClickListener(this);
        okButton.setEnabled(false);
        startButton.setOnClickListener(this);
        restartButton.setOnClickListener(this);
      
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
   @Override
   public void onClick(View view){
	   
	   int viewid = view.getId();
	   
	   switch (viewid) {
	   		case R.id.Button_ok:
	   			if(!userInputFiled.getText().toString().isEmpty()){
	   				
	   			setLevelInformation();
	   			int userInput = getuserInput();
	   			
	            if (userInput!=getRandomNumber()){
	            	
	            	userDisplayArea.setText(adviceUser(userInput));
	                tries--;
	                setLevelInformation();
	                
	                if(getTries()==0){
	                	okButton.setEnabled(false);
	                	 Toast.makeText(getApplicationContext(), "Numbers was : " +getRandomNumber()  + '\n' + "You have played : " + levels + " times", Toast.LENGTH_LONG).show();
	                	 startButton.setEnabled(true);
	                }
	                
	            }
	            else {
	            	Toast.makeText(getApplicationContext(), "Correct . Starting new Level " +levels, Toast.LENGTH_LONG).show();
	            	startNewLevel();
	            	userDisplayArea.setText("Now you have " + getTries() + " tries." + "Range has increased to :0-"+ getRange() );
	            	setLevelInformation();
	                        
	            	}
	   			}
	   			break;
	   		case R.id.start_game_btn:
	   	    	startButton.setEnabled(false);
	   	        setDefaultValues();
	   	        setRandomNumber();
	   	        userDisplayArea.setText(gameIntroduction());
	   	        setLevelInformation();
	   	        okButton.setEnabled(true);
	   			break;
	   		case R.id.btn_restart:
	   			setDefaultValues();
	   			setLevelInformation();
	   			userDisplayArea.setText(gameIntroduction());
	}
	   
   }
   
  
   public String gameIntroduction(){

       return "Hi,welcome to the game. You need to guess secret number. " +'\n'+
               "Secret number is randomly generated from 0:"+getRange()+"." +'\n'+
               "You have "+getTries()+" tries to guess the number. Good Luck";
           }
   
   private int getuserInput(){
	   int userInput = 0;
	   
	   try{
		  userInput = Integer.valueOf(userInputFiled.getText().toString()); 
		  userInputFiled.setText("");
	  }
	  catch(NumberFormatException e){
		  userInputFiled.setText("");
		  Toast.makeText(getApplicationContext(),"Please,enter correct number!",Toast.LENGTH_SHORT).show();
		  
	  }
	 
	   return userInput;
   }
   
   private void setLevelInformation(){
	        level.setText("Level : " + levels);
	        range.setText("Range : " + getRange());
	        livesRemaining.setText("Tries: " + getTries());
   }
}
