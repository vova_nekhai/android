package com.example.vova.whois;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by vova on 12/20/14.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String LOG_TAG  ="DB_HELPER";

    public DBHelper(Context context) {
        super(context, "myDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_TAG, "--- onCreate database ---");

        db.execSQL("create table whois_hosts ("
                + "id integer primary key autoincrement,"
                + "host text,"
                + "description text,"
                + "isFree boolean" + ");");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
