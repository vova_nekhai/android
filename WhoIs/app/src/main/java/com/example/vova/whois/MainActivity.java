package com.example.vova.whois;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    private static final String LOG_MANI_ACTIVITY_TAG = "MainActivity";
    private static final String LOG_ASYNC_TASK_TAG = LOG_MANI_ACTIVITY_TAG+"[ASYNC_TASK]";
    private static final String LOG_DB_TAG = LOG_MANI_ACTIVITY_TAG+"[DB_ACTIONS]";

    private Button searchButton;
    private Button advancedInfoButton;
    private Button historyButton;
    private EditText userInputFiled;
    private TextView userDisplayArea;
    private ProgressBar spinner;

    private String requestResult;
    private String userInputText;

    private boolean isDomainExisting = false;

    DBHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initButtons();
        searchButton.setOnClickListener(this);
        advancedInfoButton.setOnClickListener(this);
        historyButton.setOnClickListener(this);


    }

    private void initButtons() {
        searchButton = (Button) findViewById(R.id.search_btn);
        historyButton = (Button) findViewById(R.id.show_history_btn);
        advancedInfoButton = (Button) findViewById(R.id.advance_info_btn);
        userDisplayArea = (TextView) findViewById(R.id.userDisplayArea);
        userInputFiled = (EditText) findViewById(R.id.userInputFiled);
        spinner = (ProgressBar) findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);
        advancedInfoButton.setEnabled(false);
        dbHelper = new DBHelper(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();


        switch (id) {
            case R.id.search_btn:

                userInputText = userInputFiled.getText().toString();

                if (isValidHostname(userInputText)) {
                    userDisplayArea.setText("");
                    WhoIsSearch search = new WhoIsSearch();
                    search.execute(userInputText);
                    spinner.setVisibility(View.VISIBLE);


                } else {
                    Toast.makeText(this, "Please,enter correct hostname", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.advance_info_btn:

                Intent intent = new Intent(getApplicationContext(), ExtendedInfo.class);
                intent.putExtra("result", requestResult);
                intent.putExtra("searchHost", userInputText);
                startActivity(intent);
                break;

            case R.id.show_history_btn:
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                Cursor c = db.query("whois_hosts", null, null, null, null, null, null);

                // ставим позицию курсора на первую строку выборки
                // если в выборке нет строк, вернется false
                if (c.moveToFirst()) {

                    // определяем номера столбцов по имени в выборке
                    int idColIndex = c.getColumnIndex("id");
                    int nameColIndex = c.getColumnIndex("host");
                    int descrIndex = c.getColumnIndex("description");
                    int emailColIndex = c.getColumnIndex("isFree");

                    do {
                        // получаем значения по номерам столбцов и пишем все в лог
                        Log.d(LOG_DB_TAG,
                                "ID = " + c.getInt(idColIndex) +
                                        ", host = " + c.getString(nameColIndex) +
                                        ", description = " + c.getString(descrIndex) +
                                        ", isFree = " + c.getString(emailColIndex));
                        // переход на следующую строку
                        // а если следующей нет (текущая - последняя), то false - выходим из цикла
                    } while (c.moveToNext());
                } else
                    Log.d(LOG_DB_TAG, "0 rows");
                c.close();
                break;
        }

    }

    private boolean isValidHostname(String userInputText) {
        //TODO : make better validation
        return userInputText != null && !userInputText.trim().isEmpty();
    }

     class WhoIsSearch extends AsyncTask<String, Void, String> {

        @Override
        protected void onPostExecute(String s) {
            requestResult = s;
            userDisplayArea.setText(s);
            spinner.setVisibility(View.GONE);
            userInputFiled.setText("");
            if (!requestResult.toLowerCase().contains("no match for")) {
                advancedInfoButton.setEnabled(true);
            }

            isDomainExisting = !requestResult.toLowerCase().contains("no match for");
            Toast.makeText(getApplicationContext(),"Domain Existing : "+ isDomainExisting,Toast.LENGTH_LONG).show();
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("host", userInputText);
            cv.put("isFree", isDomainExisting);
            cv.put("description" , requestResult);
            long rowID = db.insert("whois_hosts", null, cv);
            Log.d(LOG_DB_TAG, "row inserted, ID = " + rowID + "Values :  "+userInputText +";" + isDomainExisting +";" +requestResult);
            Log.d(LOG_DB_TAG, "row inserted,= " + rowID);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            userDisplayArea.setText("Searching !");

        }

        @Override
        protected String doInBackground(String... hostname) {
            int c;
            // String whoIsSwrver = "www.registry.co.ug";


            Log.d(LOG_ASYNC_TASK_TAG, "Doing background job");
            String whoIsServer = "whois.tucows.com";
            Socket s;
            InputMethodManager imm = (InputMethodManager) getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(userInputFiled.getWindowToken(), 0);
            InputStream in;
            OutputStream out;
            StringBuilder builder = new StringBuilder();
            try {
                s = new Socket(whoIsServer, 43);
                in = s.getInputStream();
                out = s.getOutputStream();

                byte buf[] = hostname[0].getBytes();
                   //TODO  : use printwriter instead of out.write
//                PrintWriter printWriter = new PrintWriter(out,true);
//                printWriter.println(hostname);
                out.write(buf);

                while ((c = in.read()) != -1) {
                    builder.append((char) c);
                }

                s.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return builder.toString();
        }
    }

}
